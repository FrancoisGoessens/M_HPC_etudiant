#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

namespace Fibo {

  // calcule le Nieme terme de la suite de "Fibonacci modulo 42"
  // precondition : N >= 0
  int FibonacciMod42(int N) {
    int f_curr = 0;
    int f_prec = 1;
    for (int i=1; i<=N; i++) {
      int tmp = f_curr;
      f_curr = (f_curr + f_prec) % 42;
      f_prec = tmp;
    }
    return f_curr;
  }

  //////////////////////////////////////////////////////////////////////

  // fonction pour repartir les calculs
  void calculerTout(std::vector<int> &data) {
    // effectue tous les calculs
    for (int i=0; i<int(data.size()); i++) {
      data[i] = FibonacciMod42(i);
    }
  };

  std::vector<int> fiboSequentiel(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calcule les donnees sequentiellement
    calculerTout(data);
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void calculerToutThread(std::vector<int> &data, int i0, int i1) {
    // effectue tous les calculs
    for (int i=i0; i<i1; i++) {
      data[i] = FibonacciMod42(i);
    }
  }

  std::vector<int> fiboBlocs(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, par bloc
    std::thread thread1(calculerToutThread, std::ref(data), 0, data.size()/2);
    std::thread thread2(calculerToutThread, std::ref(data), data.size()/2, data.size());
    thread1.join();
    thread2.join();
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void calculerToutCycle2(std::vector<int> &data, int i0, int i1) {
    // effectue tous les calculs
    for (int i=i0; i<i1; i+=2) {
      data[i] = FibonacciMod42(i);
    }
  }
  
  std::vector<int> fiboCyclique2(int nbData) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur deux threads, cycliquement
	std::thread thread1(calculerToutCycle2, std::ref(data), 0, data.size());
	std::thread thread2(calculerToutCycle2, std::ref(data), 1, data.size());
	thread1.join();
	thread2.join();
    return data;
  }

  //////////////////////////////////////////////////////////////////////


  void calculerToutCycleN(std::vector<int> &data, int i0, int i1, int d) {
    // effectue tous les calculs
    for (int i=i0; i<i1; i+=d) {
      data[i] = FibonacciMod42(i);
    }
  }
  
  std::vector<int> fiboCycliqueN(int nbData, int nbProc) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur N threads, cycliquement
    std::vector<std::thread> T;
    
    for (int i=0; i<nbProc; i++) {
		std::thread thread1(calculerToutCycleN, std::ref(data), i, data.size(), nbProc);
		T.push_back(std::move(thread1));
	}
	for (int j=0; j<int(T.size()); j++) {
		T[j].join();
	}
    return data;
  }

  //////////////////////////////////////////////////////////////////////

  void fiboCycliqueNFake(int nbData, int nbProc) {
    // cree le tableau de donnees a calculer
    std::vector<int> data(nbData); 
    // calculer sur N threads, cycliquement, en ignorant le résultat
    std::vector<std::thread> T;
    
    for (int i=0; i<nbProc; i++) {
		std::thread thread1(calculerToutCycleN, std::ref(data), i, data.size(), nbProc);
		T.push_back(std::move(thread1));
	}
	for (int j=0; j<int(T.size()); j++) {
		T[j].join();
	}
  }

}  // namespace Fibo

