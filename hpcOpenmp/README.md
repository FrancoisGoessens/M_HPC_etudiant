# TP de HPC sur OpenMP

## utilisation générale

```
$ nix-shell

[nix-shell]$ make
...

[nix-shell]$ ./hpcOpenmpFibo.out 20
...
```

## fibo

suite de Fibonacci modulo 42


## schedule

répartition des calculs parallèles, illustration sur des images


## random

génération de nombres aléatoires en parallèle, sur des images


## filter

filtrage d'images en parallèle


