# TP de HPC sur MPI en C++

## Utilisation générale

- ouvrir un `nix-shell` :

```
$ nix-shell

[nix-shell]$
```

- compiler :

```
[nix-shell]$ make
mpic++ -std=c++11 -Wall -Wextra -Wno-literal-suffix -O2 -o hpcMpiCppHello2.out hpcMpiCppHello2.cpp
...
```

- exécuter :

```
[nix-shell]$ mpirun -n 4 hpcMpiCppHello1.out 
Hello, I am process #0
Hello, I am process #1
Hello, I am process #3
Hello, I am process #2
```


## Hello1

chaque noeud affiche un message


## Hello2

les noeuds esclaves envoient un message au noeud maitre

le noeud maitre reçoit les messages et les affiche

